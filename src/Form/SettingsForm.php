<?php

namespace Drupal\gpc\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Listing settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'gpc.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gpc_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['gpc'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('GPC status'),
      '#description' => $this->t('Implementation of next guide https://globalprivacycontrol.org/implementation'),
      '#default_value' => $config->get('gpc') || 0,
    ];

    $date = $config->get('last-update');

    $form['last-update'] = [
      '#title' => $this->t('Last Update'),
      '#type' => 'date',
      '#default_value' => $date,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('gpc', $form_state->getValue('gpc'))
      ->set('last-update', (string) $form_state->getValue('last-update'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
