<?php

namespace Drupal\gpc\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\gpc\Form\SettingsForm;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controller to return gpc.json file.
 */
class GPCJsonResponse extends ControllerBase {

  /**
   * Route to /.well-known/gpc.json.
   */
  public function result() {
    $config = $this->config(SettingsForm::SETTINGS);
    $results = [
      'gpc' => $config->get('gpc') == 1,
      'lastUpdate' => $config->get('last-update'),
    ];
    return new JsonResponse($results);
  }

}
